	.file	"udp-send.c"
	.section	.rodata
.LC0:
	.string	"127.0.0.1"
.LC1:
	.string	"socket created"
.LC2:
	.string	"bind failed"
.LC3:
	.string	"inet_aton() failed\n"
	.align 8
.LC4:
	.string	"Sending packet %d to %s port %d\n"
.LC5:
	.string	"sendto"
	.text
	.globl	sendd
	.type	sendd, @function
sendd:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$16, -96(%rbp)
	movq	$.LC0, -88(%rbp)
	movb	$1, -48(%rbp)
	movb	$1, -47(%rbp)
	movb	$1, -46(%rbp)
	movb	$1, -45(%rbp)
	movb	$-2, -44(%rbp)
	movb	$3, -43(%rbp)
	movb	$3, -42(%rbp)
	movb	$4, -41(%rbp)
	movb	$100, -40(%rbp)
	movb	$101, -39(%rbp)
	movb	$23, -38(%rbp)
	movb	$22, -37(%rbp)
	movl	$0, %edx
	movl	$2, %esi
	movl	$2, %edi
	call	socket
	movl	%eax, -92(%rbp)
	cmpl	$-1, -92(%rbp)
	jne	.L2
	movl	$.LC1, %edi
	call	puts
.L2:
	leaq	-80(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -80(%rbp)
	movl	$0, %edi
	call	htonl
	movl	%eax, -76(%rbp)
	movl	$0, %edi
	call	htons
	movw	%ax, -78(%rbp)
	leaq	-80(%rbp), %rcx
	movl	-92(%rbp), %eax
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	bind
	testl	%eax, %eax
	jns	.L3
	movl	$.LC2, %edi
	call	perror
	movl	$0, %eax
	jmp	.L9
.L3:
	leaq	-64(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -64(%rbp)
	movl	$21234, %edi
	call	htons
	movw	%ax, -62(%rbp)
	leaq	-64(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-88(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	inet_aton
	testl	%eax, %eax
	jne	.L5
	movq	stderr(%rip), %rax
	movq	%rax, %rcx
	movl	$19, %edx
	movl	$1, %esi
	movl	$.LC3, %edi
	call	fwrite
	movl	$1, %edi
	call	exit
.L5:
	movl	$0, -100(%rbp)
	jmp	.L6
.L8:
	movq	-88(%rbp), %rdx
	movl	-100(%rbp), %eax
	movl	$21234, %ecx
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movl	-96(%rbp), %ebx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	strlen
	movq	%rax, %rdi
	leaq	-64(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	movl	-92(%rbp), %eax
	movl	%ebx, %r9d
	movq	%rdx, %r8
	movl	$0, %ecx
	movq	%rdi, %rdx
	movl	%eax, %edi
	call	sendto
	cmpq	$-1, %rax
	jne	.L7
	movl	$.LC5, %edi
	call	perror
.L7:
	addl	$1, -100(%rbp)
.L6:
	cmpl	$4, -100(%rbp)
	jle	.L8
	movl	-92(%rbp), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$0, %eax
.L9:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	je	.L10
	call	__stack_chk_fail
.L10:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	sendd, .-sendd
	.globl	main
	.type	main, @function
main:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$16, -96(%rbp)
	movq	$.LC0, -88(%rbp)
	movb	$1, -48(%rbp)
	movb	$1, -47(%rbp)
	movb	$1, -46(%rbp)
	movb	$1, -45(%rbp)
	movb	$-2, -44(%rbp)
	movb	$3, -43(%rbp)
	movb	$3, -42(%rbp)
	movb	$4, -41(%rbp)
	movb	$100, -40(%rbp)
	movb	$101, -39(%rbp)
	movb	$23, -38(%rbp)
	movb	$22, -37(%rbp)
	movl	$0, %edx
	movl	$2, %esi
	movl	$2, %edi
	call	socket
	movl	%eax, -92(%rbp)
	cmpl	$-1, -92(%rbp)
	jne	.L12
	movl	$.LC1, %edi
	call	puts
.L12:
	leaq	-80(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -80(%rbp)
	movl	$0, %edi
	call	htonl
	movl	%eax, -76(%rbp)
	movl	$0, %edi
	call	htons
	movw	%ax, -78(%rbp)
	leaq	-80(%rbp), %rcx
	movl	-92(%rbp), %eax
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	bind
	testl	%eax, %eax
	jns	.L13
	movl	$.LC2, %edi
	call	perror
	movl	$0, %eax
	jmp	.L19
.L13:
	leaq	-64(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -64(%rbp)
	movl	$21234, %edi
	call	htons
	movw	%ax, -62(%rbp)
	leaq	-64(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-88(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	inet_aton
	testl	%eax, %eax
	jne	.L15
	movq	stderr(%rip), %rax
	movq	%rax, %rcx
	movl	$19, %edx
	movl	$1, %esi
	movl	$.LC3, %edi
	call	fwrite
	movl	$1, %edi
	call	exit
.L15:
	movl	$0, -100(%rbp)
	jmp	.L16
.L18:
	movq	-88(%rbp), %rdx
	movl	-100(%rbp), %eax
	movl	$21234, %ecx
	movl	%eax, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movl	-96(%rbp), %ebx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	strlen
	movq	%rax, %rdi
	leaq	-64(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	movl	-92(%rbp), %eax
	movl	%ebx, %r9d
	movq	%rdx, %r8
	movl	$0, %ecx
	movq	%rdi, %rdx
	movl	%eax, %edi
	call	sendto
	cmpq	$-1, %rax
	jne	.L17
	movl	$.LC5, %edi
	call	perror
.L17:
	addl	$1, -100(%rbp)
.L16:
	cmpl	$4, -100(%rbp)
	jle	.L18
	movl	-92(%rbp), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$0, %eax
	call	sendd
	movl	$0, %eax
.L19:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	je	.L20
	call	__stack_chk_fail
.L20:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
	.section	.note.GNU-stack,"",@progbits
