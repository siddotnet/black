/*
        demo-udp-03: udp-recv: a simple udp server
	receive udp messages

        usage:  udp-recv

        Paul Krzyzanowski
*/



#include <stdlib.h>
#include <stdio.h>
#include <time.h> // for randoms
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "port.h"

#define BUFSIZE 2048

int getrand (toprange)
{
		 srand (time (NULL));
		 return (rand () % toprange) + 1;
}

int responser(int port){

	struct sockaddr_in myaddr; 
	struct sockaddr_in remaddr;
	int fd, i, slen	= sizeof(remaddr);
	char *server 	= "127.0.0.1";

	char buf[] 	= {1, 2, 3, 4, 
			  5, 6, 7, 8, 
			  9, 10, 11, 12, 
			  13, 14, 15, 16,  
			 0 };// end_of_string 
	
	int x = (int)time(NULL);
	int y = (int)getrand(200) - 100;
	
	printf("strlen(buf) = %d\n", strlen(buf));	

	fd = socket(AF_INET, SOCK_DGRAM, 0);

	memset((char *)&myaddr, 0, sizeof(myaddr));
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	myaddr.sin_port = htons(0);

	bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr));

	memset((char *) &remaddr, 0, sizeof(remaddr));
	remaddr.sin_family = AF_INET;
	remaddr.sin_port = htons(port);

	inet_aton(server, &remaddr.sin_addr);

	sendto(fd, buf, strlen(buf), 0, (struct sockaddr *)&remaddr, slen);
	
	close(fd);

	return 0;
}


int main(int argc, char **argv)
{
	struct sockaddr_in myaddr;	/* our address */
	struct sockaddr_in remaddr;	/* remote address */
	socklen_t addrlen = sizeof(remaddr);	/* length of addresses */
	int recvlen;			/* # bytes received */
	int fd;				/* our socket */
	unsigned char buf[BUFSIZE];	/* receive buffer */


	/* create a UDP socket */

	if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("cannot create socket\n");
		return 0;
	}

	/* bind the socket to any valid IP address and a specific port */

	memset((char *)&myaddr, 0, sizeof(myaddr));
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	myaddr.sin_port = htons(SERVICE_PORT);

	if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
		perror("bind failed");
		return 0;
	}

	/* now loop, receiving data and printing what we received */
	for (;;) {
		printf("waiting on port %d\n", SERVICE_PORT);
		recvlen = recvfrom(fd, buf, BUFSIZE, 0, (struct sockaddr *)&remaddr, &addrlen);
		// get IP from remaddr; siddotnet@gmail.com
		char *rip = inet_ntoa(remaddr.sin_addr);
		int  rip2 = remaddr.sin_port;

		printf("received %d bytes from ", recvlen);
		printf("IP=%s PORT=%d", rip, rip2);
		printf("\n");
		if (recvlen > 0) {
			buf[recvlen] = 0;
			printf("received message: \"%s\"\n", buf);
			responser(16000);
		} else{
			printf("Null size.");
		}
	}
	/* never exits */
}



