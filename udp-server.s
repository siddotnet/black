	.file	"udp-server.c"
	.comm	fd,4,4
	.comm	responsePort,4,4
	.section	.rodata
.LC0:
	.string	"invalid port number: %s\n"
.LC1:
	.string	"dp:"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$0, -12(%rbp)
	movl	$21234, -8(%rbp)
	jmp	.L2
.L7:
	movl	-4(%rbp), %eax
	cmpl	$63, %eax
	je	.L3
	cmpl	$112, %eax
	je	.L4
	jmp	.L2
.L4:
	movq	optarg(%rip), %rax
	movq	%rax, %rdi
	call	atoi
	movl	%eax, -8(%rbp)
	cmpl	$1023, -8(%rbp)
	jle	.L5
	cmpl	$65535, -8(%rbp)
	jle	.L2
.L5:
	movq	optarg(%rip), %rdx
	movq	stderr(%rip), %rax
	movl	$.LC0, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, -12(%rbp)
	jmp	.L2
.L3:
	movl	$1, -12(%rbp)
	nop
.L2:
	movq	-32(%rbp), %rcx
	movl	-20(%rbp), %eax
	movl	$.LC1, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	movl	$0, %eax
	call	getopt
	movl	%eax, -4(%rbp)
	cmpl	$-1, -4(%rbp)
	jne	.L7
	cmpl	$0, -12(%rbp)
	jne	.L8
	movl	optind(%rip), %eax
	cmpl	-20(%rbp), %eax
	jge	.L9
.L8:
	movq	-32(%rbp), %rax
	movq	(%rax), %rdx
	movq	stderr(%rip), %rax
	movl	$usage.4034, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	exit
.L9:
	movl	-8(%rbp), %eax
	movl	%eax, %edi
	call	serve
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
.LC2:
	.string	"cannot create socket"
.LC3:
	.string	"bind failed"
.LC4:
	.string	"listen failed"
	.align 8
.LC5:
	.string	"server started on %s, listening on port %d\n"
.LC6:
	.string	"accept failed"
	.align 8
.LC7:
	.string	"received a connection from: %s port %d\n"
.LC8:
	.string	"received message: \"%s\"\n"
.LC9:
	.string	"\nNull length of datagram"
	.text
	.globl	serve
	.type	serve, @function
serve:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$2280, %rsp
	.cfi_offset 3, -24
	movl	%edi, -2276(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$1, -2264(%rbp)
	leaq	-2208(%rbp), %rax
	movl	$128, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	gethostname
	movl	$0, %edx
	movl	$1, %esi
	movl	$2, %edi
	call	socket
	movl	%eax, -2256(%rbp)
	cmpl	$0, -2256(%rbp)
	jns	.L12
	movl	$.LC2, %edi
	call	perror
	movl	$1, %edi
	call	exit
.L12:
	leaq	-2264(%rbp), %rdx
	movl	-2256(%rbp), %eax
	movl	$4, %r8d
	movq	%rdx, %rcx
	movl	$2, %edx
	movl	$1, %esi
	movl	%eax, %edi
	call	setsockopt
	leaq	-2240(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -2240(%rbp)
	movl	-2276(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, %edi
	call	htons
	movw	%ax, -2238(%rbp)
	movl	$0, %edi
	call	htonl
	movl	%eax, -2236(%rbp)
	leaq	-2240(%rbp), %rcx
	movl	-2256(%rbp), %eax
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	bind
	testl	%eax, %eax
	jns	.L13
	movl	$.LC3, %edi
	call	perror
	movl	$1, %edi
	call	exit
.L13:
	movl	-2256(%rbp), %eax
	movl	$5, %esi
	movl	%eax, %edi
	call	listen
	testl	%eax, %eax
	jns	.L14
	movl	$.LC4, %edi
	call	perror
	movl	$1, %edi
	call	exit
.L14:
	movl	-2276(%rbp), %edx
	leaq	-2208(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	movl	$16, -2268(%rbp)
.L19:
	jmp	.L15
.L16:
	call	__errno_location
	movl	(%rax), %eax
	cmpl	$10, %eax
	je	.L15
	call	__errno_location
	movl	(%rax), %eax
	cmpl	$85, %eax
	je	.L15
	call	__errno_location
	movl	(%rax), %eax
	cmpl	$4, %eax
	je	.L15
	movl	$.LC6, %edi
	call	perror
	movl	$1, %edi
	call	exit
.L15:
	leaq	-2268(%rbp), %rdx
	leaq	-2224(%rbp), %rcx
	movl	-2256(%rbp), %eax
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	accept
	movl	%eax, -2252(%rbp)
	cmpl	$0, -2252(%rbp)
	js	.L16
	movzwl	-2222(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, %edi
	call	ntohs
	movzwl	%ax, %ebx
	movl	-2220(%rbp), %eax
	movl	%eax, %edi
	call	inet_ntoa
	movl	%ebx, %edx
	movq	%rax, %rsi
	movl	$.LC7, %edi
	movl	$0, %eax
	call	printf
	movl	$4, -2260(%rbp)
	leaq	-2260(%rbp), %rdx
	leaq	-2224(%rbp), %rax
	leaq	4(%rax), %rcx
	leaq	-2080(%rbp), %rsi
	movl	-2248(%rbp), %eax
	movq	%rdx, %r9
	movq	%rcx, %r8
	movl	$0, %ecx
	movl	$2048, %edx
	movl	%eax, %edi
	call	recvfrom
	movl	%eax, -2244(%rbp)
	cmpl	$0, -2244(%rbp)
	jle	.L17
	movl	-2244(%rbp), %eax
	cltq
	movb	$0, -2080(%rbp,%rax)
	leaq	-2080(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC8, %edi
	movl	$0, %eax
	call	printf
	jmp	.L18
.L17:
	movl	$.LC9, %edi
	movl	$0, %eax
	call	printf
.L18:
	movzwl	-2222(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, responsePort(%rip)
	movl	-2252(%rbp), %eax
	movl	$2, %esi
	movl	%eax, %edi
	call	shutdown
	jmp	.L19
	.cfi_endproc
.LFE3:
	.size	serve, .-serve
	.section	.rodata
.LC10:
	.string	"conn(host=\"%s\", port=\"%d\")\n"
.LC11:
	.string	"getsockname failed"
.LC12:
	.string	"local port number = %d\n"
	.align 8
.LC13:
	.string	"could not obtain address of %s\n"
.LC14:
	.string	"connect failed"
	.align 8
.LC15:
	.string	"Now, trying to send parser datagram.."
	.text
	.globl	conn
	.type	conn, @function
conn:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$96, %rsp
	movq	%rdi, -88(%rbp)
	movl	%esi, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	-92(%rbp), %edx
	movq	-88(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC10, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edx
	movl	$1, %esi
	movl	$2, %edi
	call	socket
	movl	%eax, fd(%rip)
	movl	fd(%rip), %eax
	testl	%eax, %eax
	jns	.L22
	movl	$.LC2, %edi
	call	perror
	movl	$0, %eax
	jmp	.L28
.L22:
	leaq	-64(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -64(%rbp)
	movl	$0, %edi
	call	htonl
	movl	%eax, -60(%rbp)
	movl	$0, %edi
	call	htons
	movw	%ax, -62(%rbp)
	movl	fd(%rip), %eax
	leaq	-64(%rbp), %rcx
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	bind
	testl	%eax, %eax
	jns	.L24
	movl	$.LC3, %edi
	call	perror
	movl	$0, %eax
	jmp	.L28
.L24:
	movl	$16, -76(%rbp)
	movl	fd(%rip), %eax
	leaq	-76(%rbp), %rdx
	leaq	-64(%rbp), %rcx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	getsockname
	testl	%eax, %eax
	jns	.L25
	movl	$.LC11, %edi
	call	perror
	movl	$0, %eax
	jmp	.L28
.L25:
	movzwl	-62(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, %edi
	call	ntohs
	movzwl	%ax, %eax
	movl	%eax, %esi
	movl	$.LC12, %edi
	movl	$0, %eax
	call	printf
	leaq	-48(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -48(%rbp)
	movl	-92(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, %edi
	call	htons
	movw	%ax, -46(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, %rdi
	call	gethostbyname
	movq	%rax, -72(%rbp)
	cmpq	$0, -72(%rbp)
	jne	.L26
	movq	stderr(%rip), %rax
	movq	-88(%rbp), %rdx
	movl	$.LC13, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$0, %eax
	jmp	.L28
.L26:
	movq	-72(%rbp), %rax
	movl	20(%rax), %eax
	movslq	%eax, %rdx
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	leaq	-48(%rbp), %rcx
	addq	$4, %rcx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	memcpy
	movl	fd(%rip), %eax
	leaq	-48(%rbp), %rcx
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	connect
	testl	%eax, %eax
	jns	.L27
	movl	$.LC14, %edi
	call	perror
	movl	$0, %eax
	jmp	.L28
.L27:
	movl	$.LC15, %edi
	movl	$0, %eax
	call	printf
	movb	$120, -32(%rbp)
	movb	$121, -31(%rbp)
	movb	$122, -30(%rbp)
	movb	$-43, -29(%rbp)
	movb	$1, -28(%rbp)
	movb	$1, -27(%rbp)
	movb	$2, -26(%rbp)
	movb	$77, -25(%rbp)
	movb	$7, -24(%rbp)
	movb	$-1, -23(%rbp)
	movb	$-2, -22(%rbp)
	movb	$0, -21(%rbp)
	movb	$1, -20(%rbp)
	movb	$5, -19(%rbp)
	movb	$4, -18(%rbp)
	movb	$3, -17(%rbp)
	movl	fd(%rip), %eax
	cltq
	movq	%rax, %rdx
	leaq	-32(%rbp), %rax
	movq	%rdx, %rcx
	movl	$16, %edx
	movl	$1, %esi
	movq	%rax, %rdi
	call	fwrite
	movl	fd(%rip), %eax
	cltq
	movq	%rax, %rdi
	call	fclose
	movl	$1, %eax
.L28:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L29
	call	__stack_chk_fail
.L29:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	conn, .-conn
	.data
	.align 16
	.type	usage.4034, @object
	.size	usage.4034, 26
usage.4034:
	.string	"usage: %s [-d] [-p port]\n"
	.ident	"GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
	.section	.note.GNU-stack,"",@progbits
