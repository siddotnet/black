	.file	"udp-client.c"
	.section	.rodata
.LC0:
	.string	"127.0.0.1"
.LC1:
	.string	"invalid port number: %s\n"
.LC2:
	.string	"dh:p:"
.LC3:
	.string	"connecting to %s, port %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%edi, -36(%rbp)
	movq	%rsi, -48(%rbp)
	movl	$0, -28(%rbp)
	movq	$0, -8(%rbp)
	movl	$21234, -24(%rbp)
	movq	$.LC0, -16(%rbp)
	jmp	.L2
.L8:
	movl	-20(%rbp), %eax
	cmpl	$104, %eax
	je	.L3
	cmpl	$112, %eax
	je	.L4
	cmpl	$63, %eax
	je	.L5
	jmp	.L2
.L3:
	movq	optarg(%rip), %rax
	movq	%rax, -16(%rbp)
	jmp	.L2
.L4:
	movq	optarg(%rip), %rax
	movq	%rax, %rdi
	call	atoi
	movl	%eax, -24(%rbp)
	cmpl	$1023, -24(%rbp)
	jle	.L6
	cmpl	$65535, -24(%rbp)
	jle	.L2
.L6:
	movq	optarg(%rip), %rdx
	movq	stderr(%rip), %rax
	movl	$.LC1, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, -28(%rbp)
	jmp	.L2
.L5:
	movl	$1, -28(%rbp)
	nop
.L2:
	movq	-48(%rbp), %rcx
	movl	-36(%rbp), %eax
	movl	$.LC2, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	movl	$0, %eax
	call	getopt
	movl	%eax, -20(%rbp)
	cmpl	$-1, -20(%rbp)
	jne	.L8
	cmpl	$0, -28(%rbp)
	jne	.L9
	movl	optind(%rip), %eax
	cmpl	-36(%rbp), %eax
	jge	.L10
.L9:
	movq	-48(%rbp), %rax
	movq	(%rax), %rdx
	movq	stderr(%rip), %rax
	movl	$usage.3613, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$1, %edi
	call	exit
.L10:
	movl	-24(%rbp), %edx
	movq	-16(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	movl	-24(%rbp), %edx
	movq	-16(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	conn
	testl	%eax, %eax
	jne	.L11
	movl	$1, %edi
	call	exit
.L11:
	call	disconn
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.comm	fd,4,4
	.section	.rodata
.LC4:
	.string	"conn(host=\"%s\", port=\"%d\")\n"
.LC5:
	.string	"cannot create socket"
.LC6:
	.string	"bind failed"
.LC7:
	.string	"getsockname failed"
.LC8:
	.string	"local port number = %d\n"
	.align 8
.LC9:
	.string	"could not obtain address of %s\n"
.LC10:
	.string	"connect failed"
	.align 8
.LC11:
	.string	"\nNow, fwrite buffer to server.."
	.text
	.globl	conn
	.type	conn, @function
conn:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%rdi, -72(%rbp)
	movl	%esi, -76(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	-76(%rbp), %edx
	movq	-72(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edx
	movl	$1, %esi
	movl	$2, %edi
	call	socket
	movl	%eax, fd(%rip)
	movl	fd(%rip), %eax
	testl	%eax, %eax
	jns	.L14
	movl	$.LC5, %edi
	call	perror
	movl	$0, %eax
	jmp	.L20
.L14:
	leaq	-48(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -48(%rbp)
	movl	$0, %edi
	call	htonl
	movl	%eax, -44(%rbp)
	movl	$0, %edi
	call	htons
	movw	%ax, -46(%rbp)
	movl	fd(%rip), %eax
	leaq	-48(%rbp), %rcx
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	bind
	testl	%eax, %eax
	jns	.L16
	movl	$.LC6, %edi
	call	perror
	movl	$0, %eax
	jmp	.L20
.L16:
	movl	$16, -60(%rbp)
	movl	fd(%rip), %eax
	leaq	-60(%rbp), %rdx
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	getsockname
	testl	%eax, %eax
	jns	.L17
	movl	$.LC7, %edi
	call	perror
	movl	$0, %eax
	jmp	.L20
.L17:
	movzwl	-46(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, %edi
	call	ntohs
	movzwl	%ax, %eax
	movl	%eax, %esi
	movl	$.LC8, %edi
	movl	$0, %eax
	call	printf
	leaq	-32(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -32(%rbp)
	movl	-76(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, %edi
	call	htons
	movw	%ax, -30(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, %rdi
	call	gethostbyname
	movq	%rax, -56(%rbp)
	cmpq	$0, -56(%rbp)
	jne	.L18
	movq	stderr(%rip), %rax
	movq	-72(%rbp), %rdx
	movl	$.LC9, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$0, %eax
	jmp	.L20
.L18:
	movq	-56(%rbp), %rax
	movl	20(%rax), %eax
	movslq	%eax, %rdx
	movq	-56(%rbp), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	leaq	-32(%rbp), %rcx
	addq	$4, %rcx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	memcpy
	movl	fd(%rip), %eax
	leaq	-32(%rbp), %rcx
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	connect
	testl	%eax, %eax
	jns	.L19
	movl	$.LC10, %edi
	call	perror
	movl	$0, %eax
	jmp	.L20
.L19:
	movl	$.LC11, %edi
	call	puts
	movl	$1, %eax
.L20:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L21
	call	__stack_chk_fail
.L21:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	conn, .-conn
	.section	.rodata
.LC12:
	.string	"disconn()"
	.text
	.globl	disconn
	.type	disconn, @function
disconn:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$.LC12, %edi
	call	puts
	movl	fd(%rip), %eax
	movl	$2, %esi
	movl	%eax, %edi
	call	shutdown
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	disconn, .-disconn
	.data
	.align 32
	.type	usage.3613, @object
	.size	usage.3613, 42
usage.3613:
	.string	"usage: %s [-d] [-h serverhost] [-p port]\n"
	.ident	"GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
	.section	.note.GNU-stack,"",@progbits
