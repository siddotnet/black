	.file	"udp-recv.c"
	.text
	.globl	getrand
	.type	getrand, @function
getrand:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	$0, %edi
	call	time
	movl	%eax, %edi
	call	srand
	call	rand
	cltd
	idivl	-4(%rbp)
	movl	%edx, %eax
	addl	$1, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	getrand, .-getrand
	.section	.rodata
.LC0:
	.string	"127.0.0.1"
.LC1:
	.string	"strlen(buf) = %d\n"
	.text
	.globl	responser
	.type	responser, @function
responser:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -24
	movl	%edi, -116(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$16, -104(%rbp)
	movq	$.LC0, -88(%rbp)
	movb	$1, -48(%rbp)
	movb	$2, -47(%rbp)
	movb	$3, -46(%rbp)
	movb	$4, -45(%rbp)
	movb	$5, -44(%rbp)
	movb	$6, -43(%rbp)
	movb	$7, -42(%rbp)
	movb	$8, -41(%rbp)
	movb	$9, -40(%rbp)
	movb	$10, -39(%rbp)
	movb	$11, -38(%rbp)
	movb	$12, -37(%rbp)
	movb	$13, -36(%rbp)
	movb	$14, -35(%rbp)
	movb	$15, -34(%rbp)
	movb	$16, -33(%rbp)
	movb	$0, -32(%rbp)
	movl	$0, %edi
	call	time
	movl	%eax, -100(%rbp)
	movl	$200, %edi
	movl	$0, %eax
	call	getrand
	subl	$100, %eax
	movl	%eax, -96(%rbp)
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	strlen
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %edx
	movl	$2, %esi
	movl	$2, %edi
	call	socket
	movl	%eax, -92(%rbp)
	leaq	-80(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -80(%rbp)
	movl	$0, %edi
	call	htonl
	movl	%eax, -76(%rbp)
	movl	$0, %edi
	call	htons
	movw	%ax, -78(%rbp)
	leaq	-80(%rbp), %rcx
	movl	-92(%rbp), %eax
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	bind
	leaq	-64(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -64(%rbp)
	movl	-116(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, %edi
	call	htons
	movw	%ax, -62(%rbp)
	leaq	-64(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-88(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	inet_aton
	movl	-104(%rbp), %ebx
	leaq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	strlen
	movq	%rax, %rdi
	leaq	-64(%rbp), %rdx
	leaq	-48(%rbp), %rsi
	movl	-92(%rbp), %eax
	movl	%ebx, %r9d
	movq	%rdx, %r8
	movl	$0, %ecx
	movq	%rdi, %rdx
	movl	%eax, %edi
	call	sendto
	movl	-92(%rbp), %eax
	movl	%eax, %edi
	movl	$0, %eax
	call	close
	movl	$0, %eax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	je	.L5
	call	__stack_chk_fail
.L5:
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	responser, .-responser
	.section	.rodata
.LC2:
	.string	"cannot create socket\n"
.LC3:
	.string	"bind failed"
.LC4:
	.string	"waiting on port %d\n"
.LC5:
	.string	"received %d bytes from "
.LC6:
	.string	"IP=%s PORT=%d"
.LC7:
	.string	"received message: \"%s\"\n"
.LC8:
	.string	"Null size."
	.text
	.globl	main
	.type	main, @function
main:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$2144, %rsp
	movl	%edi, -2132(%rbp)
	movq	%rsi, -2144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$16, -2120(%rbp)
	movl	$0, %edx
	movl	$2, %esi
	movl	$2, %edi
	call	socket
	movl	%eax, -2116(%rbp)
	cmpl	$0, -2116(%rbp)
	jns	.L7
	movl	$.LC2, %edi
	call	perror
	movl	$0, %eax
	jmp	.L12
.L7:
	leaq	-2096(%rbp), %rax
	movl	$16, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	memset
	movw	$2, -2096(%rbp)
	movl	$0, %edi
	call	htonl
	movl	%eax, -2092(%rbp)
	movl	$21234, %edi
	call	htons
	movw	%ax, -2094(%rbp)
	leaq	-2096(%rbp), %rcx
	movl	-2116(%rbp), %eax
	movl	$16, %edx
	movq	%rcx, %rsi
	movl	%eax, %edi
	call	bind
	testl	%eax, %eax
	jns	.L9
	movl	$.LC3, %edi
	call	perror
	movl	$0, %eax
	jmp	.L12
.L9:
	movl	$21234, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	leaq	-2120(%rbp), %rcx
	leaq	-2080(%rbp), %rdx
	leaq	-2064(%rbp), %rsi
	movl	-2116(%rbp), %eax
	movq	%rcx, %r9
	movq	%rdx, %r8
	movl	$0, %ecx
	movl	$2048, %edx
	movl	%eax, %edi
	call	recvfrom
	movl	%eax, -2112(%rbp)
	movl	-2076(%rbp), %eax
	movl	%eax, %edi
	call	inet_ntoa
	movq	%rax, -2104(%rbp)
	movzwl	-2078(%rbp), %eax
	movzwl	%ax, %eax
	movl	%eax, -2108(%rbp)
	movl	-2112(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	movl	-2108(%rbp), %edx
	movq	-2104(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC6, %edi
	movl	$0, %eax
	call	printf
	movl	$10, %edi
	call	putchar
	cmpl	$0, -2112(%rbp)
	jle	.L10
	movl	-2112(%rbp), %eax
	cltq
	movb	$0, -2064(%rbp,%rax)
	leaq	-2064(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC7, %edi
	movl	$0, %eax
	call	printf
	movl	$16000, %edi
	call	responser
	jmp	.L9
.L10:
	movl	$.LC8, %edi
	movl	$0, %eax
	call	printf
	jmp	.L9
.L12:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L13
	call	__stack_chk_fail
.L13:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010"
	.section	.note.GNU-stack,"",@progbits
